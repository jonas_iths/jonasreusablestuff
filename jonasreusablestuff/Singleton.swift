//
//  Singleton.swift
//  test1
//
//  Created by Jonas Ekström on 2017-01-12.
//  Copyright © 2017 Jonas Ekström. All rights reserved.
//

import Foundation

class Singleton {
    var test:String = ""
    
    static let sharedInstance = Singleton()
    /* private init() {} */
}

